import { combineReducers } from 'redux';
import { reducer as apis} from './apis/apis-reducer';

export default combineReducers({
  apis
})