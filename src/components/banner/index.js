import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Container } from '@material-ui/core';
import Button from '../button';
import Header from '../header';

const useStyles = makeStyles(theme => ({
  banner: {
    background: 'linear-gradient(90deg, #009DDF 0%, #2F4F98 50%)',
    backgroundImage: 'url("../../static/images/header.jpg")',
    backgroundSize: 'cover',
    height: '100vh',
    // paddingTop: 56,
    color: '#ffffff',
    textAlign: 'center',
  },
  title: {
    marginBottom: 8,
  },
  subtitle: {
    marginBottom: 88,
  },
  text: {
    marginTop: 48,
  },
  container: {
    marginTop: 136,
  },
  botaoComprar: {
    marginBottom: 48,
  },
}));

export default function() {
  const classes = useStyles();

  return (
    <div className={classes.banner}>
      <Container>
        <Header />
        <Grid container className={classes.container}>
          <Grid item xs={12}>
            <Typography variant="h1" className={classes.title}>
              Quanto vale seu carro?
            </Typography>
            <Typography variant="h2" className={classes.subtitle}>
              A Volanty ajuda a descobrir o melhor valor pro seu veículo
            </Typography>

            <Button
              id="calcular-agora"
              color="secondary"
              variant="contained"
              fullWidth
              className={classes.botaoComprar}
            >
              Calcular agora
            </Button>

            <Typography variant="p">
              Nossa calculadora inteligente utiliza dados do mercado automotivo
              pra calcular o valor do seu carro.
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
