import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { Close } from '@material-ui/icons';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {
  AppBar,
  Toolbar,
  IconButton,
  Drawer,
  Avatar,
  Grid,
  List,
  Link,
  Hidden,
  ListItem,
  ListItemText,
  Divider,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { useWindowScroll, useWindowSize } from 'react-use';

import Button from '../button';

const useStyles = makeStyles(theme => ({
  header: {
    height: 56,
    maxHeight: 56,
    position: 'relative',
    background: 'transparent',
    [theme.breakpoints.up('md')]: {
      height: 'inherit',
      maxHeight: 'inherit',
      position: 'fixed',
      top: 0,
      paddingTop: 5,
      paddingBottom: 5,
      width: '100%',
      background: 'linear-gradient(rgba(0,0,0,.7),transparent)',
    },
    '& .MuiToolbar-regular': {
      height: 56,
      minHeight: 56,
      maxHeight: 56,

      [theme.breakpoints.up('md')]: {
        minHeight: 66,
        maxHeight: 66,
        height: 66,
      },
    },
  },
  fade: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  logoContent: {
    flexGrow: 1,
    padding: '10px 0',
    textAlign: 'left',
    '& img': {
      width: 94,
      display: 'block',
      [theme.breakpoints.up('md')]: {
        width: 140,
      },
    },
  },

  itemMenu: {
    marginRight: 12,
    marginLeft: 12,
    marginBottom: 0,
  },
  bgAvatar: {
    width: 44,
    height: 44,
  },
  login: {
    background: 'linear-gradient(90deg,#009ddf,#2f4f98)',
    height: 84,
    color: '#fff',
    padding: 8,

    '& .closeIcon': {
      cursor: 'pointer',
      position: 'absolute',
      fontSize: 24,
      top: 16,
      right: 10,
    },
    '& .welcome': {
      fontSize: 20,
      fontWeight: 600,
    },
    '& .welcomeText': {
      marginTop: 0,
      fontSize: 14,
    },
    '& .welcomeLink': {
      fontSize: 14,
      fontWeight: 600,
      color: '#fff',
    },
  },
  paper: {
    width: 324,
  },
  divider: {
    margin: '0 auto',
    width: '90%',
    '&:last-child': {
      backgroundColor: 'transparent',
    },
  },
  userDetails: {
    paddingLeft: 16
  }
}));
let whereScreen = 0;

export default function Header({ loadCustomerInfo, customerInfo }) {
  const classes = useStyles();
  const { y } = useWindowScroll();
  const { width, height } = useWindowSize();

  const [state, setState] = useState({
    left: false,
  });

  const [nickName, setNickName] = useState();
  const [avatarPicture, setAvatarPicture] = useState();


  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [side]: open });
  };

  useEffect(() => {
    // loadCustomerInfo();
    setNickName(sessionStorage.getItem("customerNickname"));
    setAvatarPicture(sessionStorage.getItem("customerAvatar"));
  }, []);

  // useEffect(() => {
  //   return () => {
  //     // console.log(y, window.pageYOffset);

  //     const getAltura = height;
  //     const getHeader = document.querySelector('#header .MuiToolbar-root');
  //     const getHeaderDesktop = document.querySelector('#header');

  //     const getHeaderHeight = document.querySelector('#header').offsetHeight;
  //     const getComo = document.querySelector('#como-funciona').offsetTop - 56;

  //     const getFooterHeight = document.querySelector('#footer').offsetHeight;

  //     const getAppBar = document.querySelector('#header-swipe .MuiTabs-root');
  //     const getAppBarHeight =
  //       document.querySelector('#header-swipe').offsetTop - 56;

  //     const getWhatsapp = document.querySelector('#fab-whatsapp-cta');

  //     const btnShow = document.querySelector('.btnShow');

  //     function fixarHeaderDesktop() {
  //       const getBtnHeader = document.querySelector('#btn-vender');
  //       // FIXAR MENU TOPO
  //       // MANTEM O HEADER FIXO MAS ESCONDIDO
  //       // VERIFICA SE O SCROLL É MAIOR OU IGUAL A ALTURA DO HEADER
  //       if (window.pageYOffset > getHeaderHeight) {
  //         getHeaderDesktop.classList.add('fixedDesktop');
  //       } else {
  //         getHeaderDesktop.classList.remove('fixedDesktop');
  //       }
  //       if (window.pageYOffset >= getComo) {
  //         getBtnHeader.classList.add('active');
  //       } else {
  //         getBtnHeader.classList.remove('active');
  //       }
  //     }
  //     function fixarHeader() {
  //       // FIXAR MENU TOPO
  //       // MANTEM O HEADER FIXO MAS ESCONDIDO
  //       // VERIFICA SE O SCROLL É MAIOR OU IGUAL A ALTURA DO HEADER
  //       if (window.pageYOffset > getHeaderHeight) {
  //         getHeader.classList.add('fixed');
  //       } else {
  //         getHeader.classList.remove('fixed');
  //       }
  //       // EXIBE O HEADER COM ANIMAÇÃO
  //       // VERIFICA SE O SCROLL É MAIOR OU IGUAL A ALTURA DA JANELA
  //       if (window.pageYOffset >= getAltura) {
  //         getHeader.classList.add('fixed-show');
  //       } else {
  //         getHeader.classList.remove('fixed-show');
  //       }
  //     }

  //     function fixarWhatsapp() {
  //       getWhatsapp.classList.add('fixed-show');
  //     }
  //     function removerWhatsapp() {
  //       getWhatsapp.classList.remove('fixed-show');
  //     }
  //     function fixarTopWhatsapp() {
  //       getWhatsapp.classList.add('top');
  //     }
  //     function removerTopWhatsapp() {
  //       getWhatsapp.classList.remove('top');
  //     }

  //     const getFooter = document.querySelector('#footer').offsetTop;
  //     const getComoHeight =
  //       document.querySelector('#como-funciona').offsetHeight +
  //       (getAppBarHeight - 456);
  //     if (width <= 1024) {
  //       // VERIFICA DE O USUÁRIO ESTÁ DANDO SCROLL PARA CIMA
  //       if (whereScreen > y) {
  //         fixarHeader();
  //         // VERIFICA SE A OPÇÃO DE VENDER E COMPRAR CHEGOU NO TOPO DA JANELA
  //         // E SE O SCROLL É MENOR QUE A ALTURA DO CONTAINER COMO FUNCIONA
  //         if (
  //           window.pageYOffset >= getAppBarHeight &&
  //           window.pageYOffset <= getComoHeight
  //         ) {
  //           getAppBar.classList.add('fixed-show');
  //           getAppBar.classList.remove('top');
  //           btnShow.classList.add('show');
  //           fixarTopWhatsapp();
  //         }
  //         // VERIFICA SE O SCROLL É MAIOR QUE A ALTURA DO CONTAINER COMO FUNCIONA
  //         // E SE É MAIOR QUE O PRÓXIMO SCREEN DA TELA
  //         else if (
  //           window.pageYOffset >= getComoHeight &&
  //           window.pageYOffset >= getComoHeight + 740
  //         ) {
  //           btnShow.classList.add('show');
  //           fixarTopWhatsapp();
  //         } else {
  //           getAppBar.classList.remove('fixed-show');
  //           btnShow.classList.remove('show');
  //           removerTopWhatsapp();
  //         }
  //         // FIXAR WHATSAPP
  //         if (window.pageYOffset >= getAltura) {
  //           fixarWhatsapp();
  //           if (window.pageYOffset >= getFooter - getFooterHeight - 134) {
  //             removerWhatsapp();
  //             btnShow.classList.remove('show');
  //           }
  //         } else {
  //           removerWhatsapp();
  //         }
  //       }
  //       // SCROLL PRA BAIXO SOME TODOS OS FAB'S
  //       else {
  //         getHeader.classList.remove('fixed-show');
  //         btnShow.classList.remove('show');
  //         removerTopWhatsapp();
  //         removerWhatsapp();
  //         if (window.pageYOffset >= getAppBarHeight + 56) {
  //           getAppBar.classList.add('fixed-show', 'top');
  //         }
  //         if (window.pageYOffset >= getComoHeight) {
  //           getAppBar.classList.remove('fixed-show', 'top');
  //         }
  //       }
  //       whereScreen = y;
  //     } else {
  //       fixarHeaderDesktop();
  //       fixarWhatsapp();
  //     }
  //   };
  // }, [y]);

  
  return (
    <>
      <AppBar position="static" id="header" className={classes.header}>
        <Toolbar>
          <Hidden mdUp>
            <IconButton
              edge="start"
              color="inherit"
              onClick={toggleDrawer('left', true)}
            >
              <MenuIcon />
            </IconButton>
          </Hidden>

          <a href="/" className={classes.logoContent}>
            <img
              src="https://assets.volanty.com/icons/logo.svg"
              className="logo-header"
              alt="Volanty"
            />
          </a>

          <Link
            href="/busca"
            variant="body1"
            color="inherit"
            className={classes.itemMenu}
          >
            Comprar
          </Link>
          <Hidden mdUp>
            <Link
              href="/vender.html"
              variant="body1"
              color="inherit"
              className={classes.itemMenu}
            >
              Vender
            </Link>
          </Hidden>
          <Hidden smDown>
            <Link
              href="/contato.html"
              variant="body1"
              color="inherit"
              className={classes.itemMenu}
            >
              Localizações e Contato
            </Link>
          </Hidden>
          <Hidden smDown>
            <Link
              href="/quemSomos.html"
              variant="body1"
              color="inherit"
              className={classes.itemMenu}
            >
              Sobre a Volanty
            </Link>
            <Link href="#" className={classes.itemMenu}>
              {/* <Avatar color="primary" src={customerInfo.avatar}>
                <AccountCircleIcon className={classes.bgAvatar} />
              </Avatar> */}
            </Link>
          </Hidden>
          <Button
            href="/vender.html"
            color="secondary"
            variant="outlined"
            className={clsx(classes.itemMenu, classes.fade, 'branco')}
            id="btn-vender"
          >
            Vender
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer
        open={state.left}
        onClose={toggleDrawer('left', false)}
        classes={{ paper: classes.paper }}
      >
        <div role="presentation">
          <div className={classes.login}>
            <Close
              className="closeIcon"
              onClick={toggleDrawer('left', false)}
            />
            {!nickName ? (
              <>
                <span className="welcome">Bem-vindo</span>
                <p className="welcomeText">
                  <a className="welcomeLink" href="https://www.volanty.com/profile">
                    Entre na sua conta
                </a>{' '}
                  e acompanhe seus anúncios
            </p>
              </>
            ) : (
                <>
                  <Grid container>
                    <Grid item xs={2}>
                      <Avatar color="primary" src={avatarPicture}>
                        <AccountCircleIcon className={classes.bgAvatar} />
                      </Avatar>
                    </Grid>
                    <Grid item xs={10} className={classes.userDetails}>
                      <span className="welcome">{nickName}</span>
                      <p className="welcomeText">
                        <a className="welcomeLink" href="https://www.volanty.com/profile">
                          Ver seu perfil
                      </a>
                      </p>
                    </Grid>
                  </Grid>
                </>
              )}
          </div>
          <List>
            {[
              'Comprar',
              'Vender',
              'Localizações e Contato',
              'Sobre a Volanty',
            ].map((text, index) => (
              <React.Fragment key={`menu-${index}`}>
                <ListItem button key={text} className={classes.menuListItem}>
                  <ListItemText primary={text} className="menuText" />
                </ListItem>
                <Divider className={classes.divider} />
              </React.Fragment>
            ))}
          </List>
        </div>
      </Drawer>
    </>
  );
}
