import axios from 'axios';
import { requestGetAllCavs } from './apis_requests';

const FETCH_CAVS = 'FETCH_CAVS';
const CAVS_FETCHED = 'CAVS_FETCHED';
const FETCH_CAVS_FAILED = 'FETCH_CAVS_FAILED';

const initialState = {
  cavs: [],
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CAVS_FETCHED:
      return {
        ...state,
        cavs: action.payload.cavs,
      };
    case FETCH_CAVS_FAILED:
      return {
        ...state,
        cavs: action.payload.error,
      };
    case FETCH_CAVS:
    default:
      return state;
  }
};

export const getAllCavs = async (dispatch, getState) => {
  const state = getState();
  const { cavApiUrl } = state.config;

  try {
    const request = requestGetAllCavs(cavApiUrl);
    const { data } = await axios(request);
    dispatch({
      type: CAVS_FETCHED,
      payload: {
        cavs: data,
      },
    });
  } catch (e) {
    dispatch({
      type: FETCH_CAVS_FAILED,
      payload: {
        error: e,
      },
    });
  }
};
