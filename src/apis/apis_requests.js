export function requestGetAllCavs(cavApiUrl) {
  return {
    method: GET,
    url: `${cavApiUrl}/cav`,
  };
}
