import React from 'react';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import error from '@material-ui/core/colors/red';

const Theme = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

const primaryColor = '#2F4F98';
const secondaryColor = '#009DDF';
const backgroundColorPrimary =
  'linear-gradient(90deg, #009DDF 0%, #2F4F98 50%)';

const view = createMuiTheme();
export const theme = createMuiTheme({
  typography: {
    fontWeightLight: 200,
    fontWeightRegular: 500,
    fontWeightMedium: 500,
    fontWeightBold: 800,
    fontWeightExtraBold: 1000,
    fontFamily: '"Source Sans Pro"',
    fontSizeExtraSmall: 12,
    color: '#616C8A',

    body1: {
      lineHeight: 1.4,
      fontSize: 16,
      marginBottom: 16,
    },
    body2: {
      fontWeight: 400,
      fontSize: 14,
      marginBottom: 8,
    },
    caption: {
      color: '#616C8A',
    },
    h1: {
      fontSize: 32,
      fontWeight: 900,
      [view.breakpoints.up('md')]: {
        textAlign: 'center',
      },
    },
    h2: {
      fontSize: 20,
      fontWeight: 700,
    },
    h3: {
      fontSize: 40,
      fontWeight: 700,
    },
    h4: {
      fontWeight: 700,
      fontSize: 24,
    },
    h5: {
      fontSize: 12,
      textTransform: 'uppercase',
      fontWeight: 600,
      lineHeight: 1.2,
    },
    h6: {
      color: '#616C8A',
      fontWeight: 600,
      lineHeight: 1.3,
    },
  },
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xs: 0,
      sm: 600,
      md: 1024,
      lg: 1280,
      xl: 1920,
    },
  },
  palette: {
    primary: {
      main: `${primaryColor}`,
      contrastText: '#ffffff',
    },
    secondary: {
      main: `${secondaryColor}`,
      contrastText: '#ffffff',
    },
    error: {
      light: error[300],
      main: '#EF4E22',
      dark: error[700],
      contrastText: '#ffffff',
    },
  },
  overrides: {
    MuiGrid: {
      item: {
        padding: 12,
      },
    },
    MuiContainer: {
      root: {
        paddingLeft: 0,
        paddingRight: 0,
        textAlign: 'center',

        [view.breakpoints.down(1024)]: {
          maxWidth: 408,
        },
      },
    },
    MuiAppBar: {
      root: {
        boxShadow: 'inherit',
      },
    },
    MuiAvatar: {
      root: {
        width: 44,
        height: 44,
        backgroundColor: 'transparent !important',
        border: '1px solid #ffffff',
      },
    },
    MuiTabs: {
      root: {
        backgroundColor: '#fafafa',
      },
    },
    MuiTab: {
      root: {
        color: '#000',
        fontSize: '16px !important',
        '&.Mui-selected': {
          fontWeight: 700,
        },
      },
    },
    MuiCard: {
      root: {
        maxWidth: 345,
        position: 'relative',
        marginBottom: 15,
        [view.breakpoints.down('sm')]: {
          margin: '0 auto',
        },
      },
    },
    MuiCardContent: {
      root: {
        '&:last-child': {
          paddingBottom: 8,
        },
      },
    },
    MuiTextField: {
      root: {
        '& label.Mui-focused': {
          color: `${secondaryColor}`,
        },
        '& .MuiInput-underline:after': {
          borderBottomColor: `${secondaryColor}`,
        },
        '& .MuiOutlinedInput-root': {
          '& fieldset': {
            borderRadius: 40,
          },
          '&.Mui-focused fieldset': {
            borderColor: `${secondaryColor}`,
          },
        },
      },
    },
    MuiButton: {
      root: {
        height: 'auto',
        borderRadius: 24,
        paddingTop: '11px !important',
        paddingBottom: '11px !important',
        paddingLeft: 0,
        paddingRight: 0,
        width: 143,
        fontSize: 16,
        lineHeight: 'auto',
        fontWeight: 'bold',
        outlined: 'none',
        '&.MuiButton-sizeSmall': {
          fontSize: 14,
          paddingTop: '8px !important',
          paddingBottom: '8px !important',
        },
        '&.MuiButton-sizeLarge': {
          fontSize: 18,
          paddingTop: '14px !important',
          paddingBottom: '14px !important',
        },
        '&.MuiButton-containedPrimary': {
          background: `${backgroundColorPrimary}`,
        },
        '&.MuiButton-containedSecondary': {
          background: '#ffffff',
          color: `${secondaryColor}`,
          '&:focus': {
            background: '#ffffff',
          },
          '&:hover': {
            background: '#fafafa',
          },
        },
        '&.MuiButton-outlined.branco': {
          color: '#fff',
          border: '1px solid #fff',
        },
        [view.breakpoints.up('sm')]: {
          '&$fullWidth': {
            width: 310,
          },
        },
      },
    },
  },
});

export default Theme;
