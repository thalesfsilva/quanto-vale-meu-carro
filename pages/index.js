import React from 'react';
import Head from 'next/head';
import { Grid, CssBaseline, Typography } from '@material-ui/core';
import Banner from '../src/components/banner'

const Home = () => (
  <div>
    <Head>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
      />
      <link
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900&display=swap"
        rel="stylesheet"
      />
      <link href="https://www.volanty.com" rel="canonical" />
      <title>
        Comprar e Vender seu seu carro seminovo sem se preocupar é na
        Volanty.com
      </title>

      <meta
        name="viewport"
        content="initial-scale=1.0, width=device-width"
        key="viewport"
      />

      <meta
        name="description"
        content="Comprar e Vender seu carro com transparência, preço justo e simplicidade na compra do seu seminovo. Conheça a Volanty.com e descubra um novo jeito de comprar e vender carros."
      />
      <meta
        name="og:description"
        content="Comprar e Vender seu carro com transparência, preço justo e simplicidade na compra do seu seminovo. Conheça a Volanty.com e descubra um novo jeito de comprar e vender carros."
      />
      <meta
        name="og:image"
        content="https://www.volanty.com/siteimg/volantyog.png"
      />
      <meta name="og:image:type" content="image/jpeg" />
      <meta
        name="og:title"
        content="Comprar e Vender seu seu carro seminovo sem se preocupar é na Volanty.com"
      />
      <meta name="og:url" content="https://www.volanty.com" />
      <meta name="og:type" content="article" />
      <meta name="og:locale" content="pt_BR" />
      <meta name="twitter:card" content="product" />
      <meta name="twitter:site" content="Volanty.com" />
      <meta
        name="twitter:title"
        content="Comprar e Vender seu seu carro seminovo sem se preocupar é na Volanty.com"
      />
      <meta
        name="twitter:description"
        content="Comprar e Vender seu carro com transparência, preço justo e simplicidade na compra do seu seminovo. Conheça a Volanty.com e descubra um novo jeito de comprar e vender carros."
      />
      <meta
        name="twitter:image"
        content="https://www.volanty.com/siteimg/volantyog.png"
      />
      <meta name="twitter:domain" content="volanty.com" />
    </Head>

    <>
      <CssBaseline />
      <Banner />
      <Grid>
      </Grid>
    </>
  </div>
);

export default Home;
